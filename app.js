    var request = require('request');
    var express = require('express');
    var app = express();
    var driver = require('node-phantom-simple');
    var time = require('time');
    var sha1 = require('sha1');
    var jsonfile = require('jsonfile');
    var profileFolder = 'profile/';
    var randomUseragent = require('random-useragent');
    var userAgent = randomUseragent.getRandom(); // gets a random user agent string

    app.get('/linkedin', function(req, res) {
        var url = req.query.email;
        if (!url || typeof url !== 'string') {
            throw 'You must specify a url to gather links';
        }
        var profileUrl;
        var apiKey = '&apiKey=8922366c2ce47637';
        var linkedinurl = 'https://api.fullcontact.com/v2/person.json?email=' + url + apiKey;
        request(linkedinurl, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                var output = JSON.parse(body).socialProfiles;
                for (var i = 0; i < output.length; i++) {
                    if (output[i]['type'] == 'linkedin') {
                        profileUrl = output[i]['url'];
                    }
                }

                var url = profileUrl;
                console.log(url);
                driver.create({
                    path: require('phantomjs').path
                }, function(err, browser) {
                    console.log(userAgent);
                    return browser.createPage(function(err, page) {
                        page.set('settings.userAgent', userAgent);
                        return page.open(url, function(err, status) {
                            console.log(status);
                            if (status === 'success') {

                                page.includeJs('http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js', function(err) {
                                    // jQuery Loaded.
                                    // Wait for a bit for AJAX content to load on the page. Here, we are waiting 5 seconds.
                                    setTimeout(function() {
                                        return page.evaluate(function() {
                                            //Get what you want from the page using jQuery. A good way is to populate an object with all the jQuery commands that you need and then return the object.
                                            var title, section, element, itemElement, temp;
                                            var name = $('#name').text() + $('p.headline:first').text();
                                            // var html = $('body').html();
                                            var profile = {
                                                name: $('#name').text(),
                                                profilePic: $('.profile-picture').find('img').attr('src') || '',
                                                headline: $('p.headline:first').text(),
                                                connections: $('.member-connections').children('strong').text(),
                                            };
                                            $('#demographics').find('dt').each(function() {
                                                profile[($(this).text() || '').trim().toLowerCase()] = ($(this).next('dd').text() || '').trim();
                                            });

                                            $('#top-card').find('table').find('tr').each(function() {
                                                title = ($(this).children('th').children('a').text() || '').trim().toLowerCase();
                                                if (title) {
                                                    if ($(this).children('td').children('ol').length) {
                                                        profile[title] = [];
                                                        $(this).children('td').find('li').each(function() {
                                                            profile[title].push({
                                                                name: ($(this).children().text() || $(this).text() || '').trim(),
                                                                link: $(this).find('a').attr('href') || ''
                                                            });
                                                        });
                                                    } else {
                                                        profile[title] = ($(this).children('td').children('strong').eq(0).text() || '').trim();
                                                    }
                                                }
                                            });

                                            section = $('#activity').find('li');
                                            if (section.length) {
                                                profile.posts = [];
                                                section.each(function() {
                                                    profile.posts.push({
                                                        name: ($(this).find('.influencer-post-title').text() || '').trim(),
                                                        link: $(this).find('a').eq(0).attr('href') || '',
                                                        image: $(this).find('img').attr('src') || '',
                                                        time: ($(this).find('.influencer-post-published').text() || '').trim()
                                                    });
                                                });
                                            }

                                            section = $('#groups-container').find('li').not('.see-more').not('.see-less');
                                            if (section.length) {
                                                profile.groups = [];
                                                section.each(function() {
                                                    itemElement = $(this).find('.group-link');
                                                    profile.groups.push({
                                                        name: (itemElement.text() || '').trim(),
                                                        link: itemElement.attr('href') || '',
                                                        image: $(this).find('img').attr('src') || '',
                                                        members: ($(this).find('.groups-stats').text() || '').trim()
                                                    });
                                                });
                                            }

                                            //summary
                                            section = $('section#summary').find('p:first').text();
                                            if (section.length) {
                                                profile.summary = [];
                                                profile.summary.push(section);
                                            }

                                            //expretirnce
                                            section = $('section#experience');
                                            if (section.length) {
                                                profile.experience = [];
                                                section.find('li').each(function() {
                                                    company = {
                                                        companyName: $(this).find('h5.item-subtitle').text(),
                                                        position: $(this).find('h4.item-title').text(),
                                                        timeDuration: $(this).find('.date-range').text()
                                                    };
                                                    profile.experience.push(company);
                                                });
                                            }

                                            // languages
                                            section = $('section#languages');
                                            if (section.length) {
                                                profile.languages = [];
                                                section.find('li').each(function() {
                                                    language = {
                                                        name: $(this).find('h4').text(),
                                                        proficiency: $(this).find('.proficiency').text()
                                                    };
                                                    profile.languages.push(language);
                                                });
                                            }

                                            //skill
                                            section = $('section#skills');
                                            if (section.length) {
                                                profile.skills = [];
                                                section.find('li').each(function() {
                                                    profile.skills.push($(this).text());
                                                });
                                            }

                                            // education
                                            section = $('section#education');
                                            if (section.length) {
                                                profile.education = [];
                                                section.find('li').each(function() {
                                                    education = {
                                                        location: $(this).find('h4.item-title').text(),
                                                        qualifiction: $(this).find('h5 span:first').text(),
                                                        timeDuration: $(this).find('.date-range').text(),
                                                        activity: $(this).find('.description p').text()
                                                    };
                                                    profile.education.push(education);
                                                });
                                            }

                                            //interests
                                            section = $('section#interests');
                                            if (section.length) {
                                                profile.interests = [];
                                                section.find('li').each(function() {
                                                    profile.interests.push($(this).text());
                                                });
                                            }
                                            //volunteering
                                            section = $('section#volunteering');
                                            if (section.length) {
                                                profile.volunteering = [];
                                                section.find('li').each(function() {
                                                    profile.volunteering.push($('li').text());
                                                });
                                            }
                                            
                                            var data = [];
                                            data[0] = profile;
                                            data[1] = name;
                                            return data;

                                        }, function(err, data) {

                                            var now = new time.Date();
                                            var string = sha1(now);
                                            var fileName = profileFolder + data[1] + '_' + string + '.json';
                                            jsonfile.writeFile(fileName, data[0], function(err) {
                                                console.error(err)
                                            });
                                            res.send(data[0]);
                                            console.log("success");
                                            browser.exit();

                                        });

                                    });

                                });

                            } else {
                                res.send("without signin profile view blocked");
                            }

                        });
                    });
                });
            }else{
                res.send({
                    statusCode :response.statusCode,
                    error : error
                });
            }
        });

    });

    app.listen('3000', function() {
        console.log('running on 3000...');
    });